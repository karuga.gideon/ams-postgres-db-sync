/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ams.springboot.db_sync.models;

import java.math.BigInteger;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Gideon Karuga.
 */
@Entity
@Table(name = "biometrics")
public class Biometrics {

    @Id
    @Column(name = "id")
    private BigInteger id;

    @Column(name = "log_id", length = 100)
    private String logId;

    // @SequenceGenerator(name = "staff_att_seq", sequenceName = "staff_att_seq", allocationSize = 1)
    // @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "staff_att_seq")
    @Column(name = "username_id", length = 40)
    private String usernameId;

    @NotNull
    @Column(name = "finger_selected", length = 100)
    private String fingerSelected;

    // @Lob
    @Column(name = "finger_image")
    public byte[] fingerImage;

    @Column(name = "is_user_clocked")
    private boolean isUserClocked;

    @Column(name = "ams_username")
    private String amsUsername;

    @Column(name = "company_id")
    private int companyId;

    @Column(name = "user_name")
    private String userName;

    public BigInteger getId() {
        return id;
    }

    public void setId(BigInteger Id) {
        this.id = Id;
    }

    public String getLogId() {
        return logId;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public String getUsernameId() {
        return usernameId;
    }

    public void setUsernameId(String usernameId) {
        this.usernameId = usernameId;
    }

    public String getFingerSelected() {
        return fingerSelected;
    }

    public void setFingerSelected(String fingerSelected) {
        this.fingerSelected = fingerSelected;
    }

    public byte[] getFingerImage() {
        return fingerImage;
    }

    public void setFingerImage(byte[] fingerImage) {
        this.fingerImage = fingerImage;
    }

    public boolean isIsUserClocked() {
        return isUserClocked;
    }

    public void setIsUserClocked(boolean isUserClocked) {
        this.isUserClocked = isUserClocked;
    }

    public String getAmsUsername() {
        return amsUsername;
    }

    public void setAmsUsername(String amsUsername) {
        this.amsUsername = amsUsername;
    }

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

}
