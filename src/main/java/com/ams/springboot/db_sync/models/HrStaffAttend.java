/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ams.springboot.db_sync.models;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author Gideon Karuga
 *
 * )
 *
 *
 */
@Entity
@Table(name = "hr_staff_attend")
public class HrStaffAttend implements Serializable {

    @Id
    // @SequenceGenerator(name = "staff_att_seq", sequenceName = "staff_att_seq", allocationSize = 1)
    // @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "staff_att_seq")
    @Column(name = "log_id", length = 20)
    private String logId;

    @Column(name = "staff_no", length = 50)
    private String staffNo;

    @NotEmpty
    @NotNull
    @Column(name = "staff_name", length = 100)
    private String staffName;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "attend_time")
    private Date attendTime;

    @Column(name = "dept", length = 50)
    private String dept;

    @Column(name = "log_name", length = 50)
    private String logName;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "clock_out")
    private Date clockOut;

    @Column(name = "checked_out")
    private boolean checkedOut;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "end_time")
    private Date endTime;

    @Column(name = "comp_name")
    private String compName;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "log_out")
    private Date logOut;

    @Column(name = "sms_lateness")
    private boolean smsLateness;

    @Column(name = "sms_earlybird")
    private boolean smsEarlybird;

    @Column(name = "automated_clockin")
    private boolean automatedClockin;

    @Column(name = "pay_exempt")
    private boolean payExempt;

    @Column(name = "edit_reason")
    private String editReason;

    @Column(name = "mac_address")
    private String macAddress;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "input_date")
    private Date inputDate;

    @Column(name = "company_id")
    private int companyId;

    @Column(name = "is_synced")
    private boolean isSynced;

    public String getLogId() {
        return logId;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public String getStaffNo() {
        return staffNo;
    }

    public void setStaffNo(String staffNo) {
        this.staffNo = staffNo;
    }

    public String getStaffName() {
        return staffName;
    }

    public void setStaffName(String staffName) {
        this.staffName = staffName;
    }

    public Date getAttendTime() {
        return attendTime;
    }

    public void setAttendTime(Date attendTime) {
        this.attendTime = attendTime;
    }

    public String getDept() {
        return dept;
    }

    public void setDept(String dept) {
        this.dept = dept;
    }

    public String getLogName() {
        return logName;
    }

    public void setLogName(String logName) {
        this.logName = logName;
    }

    public Date getClockOut() {
        return clockOut;
    }

    public void setClockOut(Date clockOut) {
        this.clockOut = clockOut;
    }

    public boolean getCheckedOut() {
        return checkedOut;
    }

    public String getCompName() {
        return compName;
    }

    public void setCompName(String compName) {
        this.compName = compName;
    }

    public Date getLogOut() {
        return logOut;
    }

    public void setLogOut(Date logOut) {
        this.logOut = logOut;
    }

    public boolean isSmsLateness() {
        return smsLateness;
    }

    public void setSmsLateness(boolean smsLateness) {
        this.smsLateness = smsLateness;
    }

    public boolean isSmsEarlybird() {
        return smsEarlybird;
    }

    public void setSmsEarlybird(boolean smsEarlybird) {
        this.smsEarlybird = smsEarlybird;
    }

    public boolean isAutomatedClockin() {
        return automatedClockin;
    }

    public void setAutomatedClockin(boolean automatedClockin) {
        this.automatedClockin = automatedClockin;
    }

    public boolean isPayExempt() {
        return payExempt;
    }

    public void setPayExempt(boolean payExempt) {
        this.payExempt = payExempt;
    }

    public String getEditReason() {
        return editReason;
    }

    public void setEditReason(String editReason) {
        this.editReason = editReason;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    public Date getInputDate() {
        return inputDate;
    }

    public void setInputDate(Date inputDate) {
        this.inputDate = inputDate;
    }

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public void setCheckedOut(boolean checkedOut) {
        this.checkedOut = checkedOut;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public boolean isIsSynced() {
        return isSynced;
    }

    public void setIsSynced(boolean isSynced) {
        this.isSynced = isSynced;
    }

}
