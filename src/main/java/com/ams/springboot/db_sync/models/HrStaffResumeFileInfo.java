/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ams.springboot.db_sync.models;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author asus
 */
@Entity
@Table(name = "hr.hr_staffresume_fileinfo")
public class HrStaffResumeFileInfo {

    @Id
    // @SequenceGenerator(name = "staff_att_seq", sequenceName = "staff_att_seq", allocationSize = 1)
    // @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "staff_att_seq")
    @Column(name = "hrm_id", length = 100)
    private String hrmId;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "interview_date")
    private Date interviewDate;

    @Column(name = "first_name", length = 200)
    private String firstName;

    @Column(name = "second_name", length = 200)
    private String secondName;

    @Column(name = "others", length = 200)
    private String others;

    @Column(name = "staff_id")
    private String staffId;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "empmt_date")
    private Date empmtdate;

    @Column(name = "field_of_spec")
    private String fieldOfSpec;

    @Column(name = "position_app", length = 150)
    private String positionApp;

    @Column(name = "dept", length = 200)
    private String dept;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "birth_date")
    private Date birthDate;

    @Column(name = "current_residence", length = 80)
    private String currentResidence;

    @Column(name = "national_id_no")
    private String nationalIdNo;

    @Column(name = "passport_no")
    private String passportNo;

    @Column(name = "marital_status", length = 20)
    private String maritalStatus;

    @Column(name = "religion")
    private String religion;

    @Column(name = "notice_period")
    private String noticePeriod;

    @Column(name = "ethnicity", length = 20)
    private String ethnicity;

    @Column(name = "gender", length = 10)
    private String gender;

    @Column(name = "helb", length = 20)
    private String helb;

    @Column(name = "nssf_no", length = 40)
    private String nssfNo;

    @Column(name = "nhif_no", length = 40)
    private String nhifNo;

    @Column(name = "postal_add")
    private String postalAdd;

    @Column(name = "postal_code")
    private String postalCode;

    @Column(name = "email")
    private String email;

    @Column(name = "designation", length = 200)
    private String designation;

    @Column(name = "section", length = 100)
    private String section;

    @Column(name = "job_group", length = 100)
    private String jobGroup;

    @Column(name = "jgrade", length = 30)
    private String jgrade;

    @Column(name = "nationality", length = 200)
    private String nationality;

    @Column(name = "birth_place")
    private String birthPlace;

    @Column(name = "birth_district")
    private String birthDistrict;

    @Column(name = "birth_town", length = 200)
    private String birthTown;

    @Column(name = "pin_no", length = 20)
    private String pinNo;

    @Column(name = "mobile", length = 200)
    private String mobile;

    @Column(name = "mobile2")
    private String mobile2;

    @Column(name = "emer_contnames")
    private String emerContnames;

    @Column(name = "emer_relationship")
    private String emerRelationship;

    @Column(name = "staff_category", length = 100)
    private String staffCategory;

    @Column(name = "leave_category", length = 100)
    private String leaveCategory;

    @Column(name = "emer_mobile")
    private String emerMobile;

    @Column(name = "emer_email")
    private String emerEmail;

    @Column(name = "emer_postaladd")
    private String emerPostaladd;

    @Column(name = "emer_postalcode")
    private String emerPostalcode;

    @Column(name = "emer_residence", length = 200)
    private String emerResidence;

    @Column(name = "consultant")
    private boolean consultant;

    @Column(name = "system_rights")
    private boolean systemRights;

    @Column(name = "contract_renew")
    private boolean contractRenew;

    @Column(name = "contract_termination")
    private boolean contractRermination;

    @Column(name = "contract_create")
    private boolean contractCreate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "report_date")
    private Date reportDate;

    @Column(name = "payroll_status")
    private boolean payrollStatus;

    @Column(name = "staff_status", length = 100)
    private String staffStatus;

//    @Column(name = "hrm_id", length = 100)
//    private String hrmId;

    @Column(name = "confirmed")
    private boolean confirmed;

    @Column(name = "suspend")
    private boolean suspend;

    @Column(name = "onleave")
    private boolean onleave;

    @Column(name = "standard_shift")
    private boolean standardShift;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "input_date")
    private Date inputDate;

    @Column(name = "user_name")
    private String userName;

    @Column(name = "approved")
    private boolean approved;

    @Column(name = "approval_status")
    private int approvalStatus;

    @Column(name = "work_email", length = 200)
    private String workEmail;

    @Column(name = "checked")
    private boolean checked;

    @Column(name = "edited_by")
    private String editedBy;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "edited_date")
    private Date editedDate;

    @Column(name = "hod")
    private boolean hod;

    @Column(name = "supervisor")
    private String supervisor;

    @Column(name = "special")
    private boolean special;

    @Column(name = "company_id")
    private int companyId;

    @Column(name = "title", length = 200)
    private String title;

    @Column(name = "job")
    private String job;

    @Column(name = "emp_category")
    private String empCategory;

    @Column(name = "bunit")
    private String bunit;

    @Column(name = "payroll", length = 200)
    private String payroll;

    @Column(name = "emp_code", length = 200)
    private String empCode;

    @Column(name = "branch_enabled")
    private boolean branchEnabled;

    public String getHrmid() {
        return hrmId;
    }

    public void setHrmid(String hrmId) {
        this.hrmId = hrmId;
    }

    public Date getInterviewDate() {
        return interviewDate;
    }

    public void setInterviewDate(Date interviewDate) {
        this.interviewDate = interviewDate;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getOthers() {
        return others;
    }

    public void setOthers(String others) {
        this.others = others;
    }

    public String getStaffId() {
        return staffId;
    }

    public void setStaffId(String staffId) {
        this.staffId = staffId;
    }

    public Date getEmpmtdate() {
        return empmtdate;
    }

    public void setEmpmtdate(Date empmtdate) {
        this.empmtdate = empmtdate;
    }

    public String getFieldOfSpec() {
        return fieldOfSpec;
    }

    public void setFieldOfSpec(String fieldOfSpec) {
        this.fieldOfSpec = fieldOfSpec;
    }

    public String getPositionApp() {
        return positionApp;
    }

    public void setPositionApp(String positionApp) {
        this.positionApp = positionApp;
    }

    public String getDept() {
        return dept;
    }

    public void setDept(String dept) {
        this.dept = dept;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getCurrentResidence() {
        return currentResidence;
    }

    public void setCurrentResidence(String currentResidence) {
        this.currentResidence = currentResidence;
    }

    public String getNationalIdNo() {
        return nationalIdNo;
    }

    public void setNationalIdNo(String nationalIdNo) {
        this.nationalIdNo = nationalIdNo;
    }

    public String getPassportNo() {
        return passportNo;
    }

    public void setPassportNo(String passportNo) {
        this.passportNo = passportNo;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public String getNoticePeriod() {
        return noticePeriod;
    }

    public void setNoticePeriod(String noticePeriod) {
        this.noticePeriod = noticePeriod;
    }

    public String getEthnicity() {
        return ethnicity;
    }

    public void setEthnicity(String ethnicity) {
        this.ethnicity = ethnicity;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getHelb() {
        return helb;
    }

    public void setHelb(String helb) {
        this.helb = helb;
    }

    public String getNssfNo() {
        return nssfNo;
    }

    public void setNssfNo(String nssfNo) {
        this.nssfNo = nssfNo;
    }

    public String getNhifNo() {
        return nhifNo;
    }

    public void setNhifNo(String nhifNo) {
        this.nhifNo = nhifNo;
    }

    public String getPostalAdd() {
        return postalAdd;
    }

    public void setPostalAdd(String postalAdd) {
        this.postalAdd = postalAdd;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public String getJobGroup() {
        return jobGroup;
    }

    public void setJobGroup(String jobGroup) {
        this.jobGroup = jobGroup;
    }

    public String getJgrade() {
        return jgrade;
    }

    public void setJgrade(String jgrade) {
        this.jgrade = jgrade;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getBirthPlace() {
        return birthPlace;
    }

    public void setBirthPlace(String birthPlace) {
        this.birthPlace = birthPlace;
    }

    public String getBirthDistrict() {
        return birthDistrict;
    }

    public void setBirthDistrict(String birthDistrict) {
        this.birthDistrict = birthDistrict;
    }

    public String getBirthTown() {
        return birthTown;
    }

    public void setBirthTown(String birthTown) {
        this.birthTown = birthTown;
    }

    public String getPinNo() {
        return pinNo;
    }

    public void setPinNo(String pinNo) {
        this.pinNo = pinNo;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getMobile2() {
        return mobile2;
    }

    public void setMobile2(String mobile2) {
        this.mobile2 = mobile2;
    }

    public String getEmerContnames() {
        return emerContnames;
    }

    public void setEmerContnames(String emerContnames) {
        this.emerContnames = emerContnames;
    }

    public String getEmerRelationship() {
        return emerRelationship;
    }

    public void setEmerRelationship(String emerRelationship) {
        this.emerRelationship = emerRelationship;
    }

    public String getStaffCategory() {
        return staffCategory;
    }

    public void setStaffCategory(String staffCategory) {
        this.staffCategory = staffCategory;
    }

    public String getLeaveCategory() {
        return leaveCategory;
    }

    public void setLeaveCategory(String leaveCategory) {
        this.leaveCategory = leaveCategory;
    }

    public String getEmerMobile() {
        return emerMobile;
    }

    public void setEmerMobile(String emerMobile) {
        this.emerMobile = emerMobile;
    }

    public String getEmerEmail() {
        return emerEmail;
    }

    public void setEmerEmail(String emerEmail) {
        this.emerEmail = emerEmail;
    }

    public String getEmerPostaladd() {
        return emerPostaladd;
    }

    public void setEmerPostaladd(String emerPostaladd) {
        this.emerPostaladd = emerPostaladd;
    }

    public String getEmerPostalcode() {
        return emerPostalcode;
    }

    public void setEmerPostalcode(String emerPostalcode) {
        this.emerPostalcode = emerPostalcode;
    }

    public String getEmerResidence() {
        return emerResidence;
    }

    public void setEmerResidence(String emerResidence) {
        this.emerResidence = emerResidence;
    }

    public boolean isConsultant() {
        return consultant;
    }

    public void setConsultant(boolean consultant) {
        this.consultant = consultant;
    }

    public boolean isSystemRights() {
        return systemRights;
    }

    public void setSystemRights(boolean systemRights) {
        this.systemRights = systemRights;
    }

    public boolean isContractRenew() {
        return contractRenew;
    }

    public void setContractRenew(boolean contractRenew) {
        this.contractRenew = contractRenew;
    }

    public boolean isContractRermination() {
        return contractRermination;
    }

    public void setContractRermination(boolean contractRermination) {
        this.contractRermination = contractRermination;
    }

    public boolean isContractCreate() {
        return contractCreate;
    }

    public void setContractCreate(boolean contractCreate) {
        this.contractCreate = contractCreate;
    }

    public Date getReportDate() {
        return reportDate;
    }

    public void setReportDate(Date reportDate) {
        this.reportDate = reportDate;
    }

    public boolean isPayrollStatus() {
        return payrollStatus;
    }

    public void setPayrollStatus(boolean payrollStatus) {
        this.payrollStatus = payrollStatus;
    }

    public String getStaffStatus() {
        return staffStatus;
    }

    public void setStaffStatus(String staffStatus) {
        this.staffStatus = staffStatus;
    }

    public String getHrmId() {
        return hrmId;
    }

    public void setHrmId(String hrmId) {
        this.hrmId = hrmId;
    }

    public boolean isConfirmed() {
        return confirmed;
    }

    public void setConfirmed(boolean confirmed) {
        this.confirmed = confirmed;
    }

    public boolean isSuspend() {
        return suspend;
    }

    public void setSuspend(boolean suspend) {
        this.suspend = suspend;
    }

    public boolean isOnleave() {
        return onleave;
    }

    public void setOnleave(boolean onleave) {
        this.onleave = onleave;
    }

    public boolean isStandardShift() {
        return standardShift;
    }

    public void setStandardShift(boolean standardShift) {
        this.standardShift = standardShift;
    }

    public Date getInputDate() {
        return inputDate;
    }

    public void setInputDate(Date inputDate) {
        this.inputDate = inputDate;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public boolean isApproved() {
        return approved;
    }

    public void setApproved(boolean approved) {
        this.approved = approved;
    }

    public int getApprovalStatus() {
        return approvalStatus;
    }

    public void setApprovalStatus(int approvalStatus) {
        this.approvalStatus = approvalStatus;
    }

    public String getWorkEmail() {
        return workEmail;
    }

    public void setWorkEmail(String workEmail) {
        this.workEmail = workEmail;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public String getEditedBy() {
        return editedBy;
    }

    public void setEditedBy(String editedBy) {
        this.editedBy = editedBy;
    }

    public Date getEditedDate() {
        return editedDate;
    }

    public void setEditedDate(Date editedDate) {
        this.editedDate = editedDate;
    }

    public boolean isHod() {
        return hod;
    }

    public void setHod(boolean hod) {
        this.hod = hod;
    }

    public String getSupervisor() {
        return supervisor;
    }

    public void setSupervisor(String supervisor) {
        this.supervisor = supervisor;
    }

    public boolean isSpecial() {
        return special;
    }

    public void setSpecial(boolean special) {
        this.special = special;
    }

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getEmpCategory() {
        return empCategory;
    }

    public void setEmpCategory(String empCategory) {
        this.empCategory = empCategory;
    }

    public String getBunit() {
        return bunit;
    }

    public void setBunit(String bunit) {
        this.bunit = bunit;
    }

    public String getPayroll() {
        return payroll;
    }

    public void setPayroll(String payroll) {
        this.payroll = payroll;
    }

    public String getEmpCode() {
        return empCode;
    }

    public void setEmpCode(String empCode) {
        this.empCode = empCode;
    }

    public boolean isBranchEnabled() {
        return branchEnabled;
    }

    public void setBranchEnabled(boolean branchEnabled) {
        this.branchEnabled = branchEnabled;
    }

}
