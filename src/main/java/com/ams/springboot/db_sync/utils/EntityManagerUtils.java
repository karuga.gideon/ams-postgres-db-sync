package com.ams.springboot.db_sync.utils;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.support.JpaRepositoryFactory;
import org.springframework.stereotype.Component;

@Component
public class EntityManagerUtils {

    @Autowired
    @Qualifier("localEntityManager")
    private EntityManager localDatabase;

    @Autowired
    @Qualifier("remoteEntityManager")
    private EntityManager remoteDatabase;

    public EntityManager getEm(String url) {
        if (url.contains("local")) {
            return localDatabase;
        }
        if (url.contains("remote")) {
            return remoteDatabase;
        }
        return localDatabase;
    }

    public JpaRepositoryFactory getJpaFactory(String url) {
        return new JpaRepositoryFactory(getEm(url));
    }

}
