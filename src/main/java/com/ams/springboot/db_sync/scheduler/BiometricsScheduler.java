/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ams.springboot.db_sync.scheduler;

import com.ams.springboot.db_sync.models.Biometrics;
import com.ams.springboot.db_sync.repository.BiometricsRepository;
import java.math.BigInteger;
import java.util.List;
import javax.persistence.EntityManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.support.JpaRepositoryFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Gideon Karuga
 */
@Controller
@Transactional
public class BiometricsScheduler {

    private static final org.apache.log4j.Logger LOGGER = org.apache.log4j.Logger.getLogger(BiometricsScheduler.class);

    @Autowired
    @Qualifier("localEntityManager")
    private EntityManager localDatabase;

    @Autowired
    @Qualifier("remoteEntityManager")
    private EntityManager remoteDatabase;

    @Scheduled(initialDelay = 1000, fixedRate = 2000)
    public void syncHrStaffAttenDbRemoteToLocal() {

        LOGGER.info("Syncing biometrics [Remote to Local]....");

        // Get remote data count 
        long remoteRecordsCount = remoteRecordsCount();

        // Get local data count 
        long localRecordsCount = localRecordsCount();

        LOGGER.info("remoteRecordsCount : " + remoteRecordsCount + ", localRecordsCount : " + localRecordsCount);

        // If remote db has more records, sync to local 
        if (remoteRecordsCount > localRecordsCount) {
            LOGGER.info("Syncing biometrics data...");

            // Get all remote records 
            List<Biometrics> allRemoteRecords = getAllRemoteRecords();
            // Check whether local record of the same exists
            allRemoteRecords.forEach((biometric) -> {
                // If it does not exist, save it to local db
                Biometrics localRecord = getLocalRecord(biometric.getId());
                if (localRecord == null) {
                    BigInteger savedLogId = saveRemoteRecordToLocalMachine(biometric);
                    LOGGER.info("Synced biometric data >>> " + savedLogId);
                }
            });

        }
    }
    
    @Scheduled(initialDelay = 1000, fixedRate = 2000)
    public void syncHrStaffAttenDbLocalToRemote() {

        LOGGER.info("Syncing biometrics [Local to Remote]....");

        // Get remote data count 
        long remoteRecordsCount = remoteRecordsCount();

        // Get local data count 
        long localRecordsCount = localRecordsCount();

        LOGGER.info("remoteRecordsCount : " + remoteRecordsCount + ", localRecordsCount : " + localRecordsCount);

        // If remote db has more records, sync to local 
        if (localRecordsCount  > remoteRecordsCount) {
            LOGGER.info("Syncing biometrics data...");

            // Get all remote records 
            List<Biometrics> allLocalRecords = getAllLocalRecords();
            // Check whether local record of the same exists
            allLocalRecords.forEach((biometric) -> {
                // If it does not exist, save it to local db
                Biometrics remoteRecord = getRemoteRecord(biometric.getId());
                if (remoteRecord == null) {
                    BigInteger savedLogId = saveLocalRecordToRemoteMachine(biometric);
                    LOGGER.info("Synced biometric data >>> " + savedLogId);
                }
            });

        }
    }

    // Save remote record to local server 
    public BigInteger saveRemoteRecordToLocalMachine(Biometrics biometric) {

        JpaRepositoryFactory factory = new JpaRepositoryFactory(localDatabase);
        BiometricsRepository repository = factory.getRepository(BiometricsRepository.class);
        
        Biometrics newBiometric = new Biometrics();

        // Biometrics lastBiometric = repository.findFirst1ByIdOrderByIdDesc();
                
        newBiometric.setId(biometric.getId());
        newBiometric.setLogId(biometric.getLogId());
        newBiometric.setUsernameId(biometric.getUsernameId());
        newBiometric.setFingerSelected(biometric.getFingerSelected());
        newBiometric.setFingerImage(biometric.getFingerImage());
        newBiometric.setIsUserClocked(biometric.isIsUserClocked());
        newBiometric.setAmsUsername(biometric.getAmsUsername());
        newBiometric.setCompanyId(biometric.getCompanyId());
        newBiometric.setUserName(biometric.getUserName());

        
        repository.save(newBiometric);

        return newBiometric.getId();

    }
    
    public BigInteger saveLocalRecordToRemoteMachine(Biometrics biometric) {

        JpaRepositoryFactory factory = new JpaRepositoryFactory(remoteDatabase);
        BiometricsRepository repository = factory.getRepository(BiometricsRepository.class);
        
        Biometrics newBiometric = new Biometrics();

        // Biometrics lastBiometric = repository.findFirst1ByIdOrderByIdDesc();
                
        newBiometric.setId(biometric.getId());
        newBiometric.setLogId(biometric.getLogId());
        newBiometric.setUsernameId(biometric.getUsernameId());
        newBiometric.setFingerSelected(biometric.getFingerSelected());
        newBiometric.setFingerImage(biometric.getFingerImage());
        newBiometric.setIsUserClocked(biometric.isIsUserClocked());
        newBiometric.setAmsUsername(biometric.getAmsUsername());
        newBiometric.setCompanyId(biometric.getCompanyId());
        newBiometric.setUserName(biometric.getUserName());

        
        repository.save(newBiometric);

        return newBiometric.getId();

    }

    
    public Biometrics getLocalRecord(BigInteger logId) {
        JpaRepositoryFactory factory = new JpaRepositoryFactory(localDatabase);
        BiometricsRepository repository = factory.getRepository(BiometricsRepository.class);
        Biometrics biometric = repository.findById(logId);
        return biometric;
    }
    
    public Biometrics getRemoteRecord(BigInteger logId) {
        JpaRepositoryFactory factory = new JpaRepositoryFactory(remoteDatabase);
        BiometricsRepository repository = factory.getRepository(BiometricsRepository.class);
        Biometrics biometric = repository.findById(logId);
        return biometric;
    }

    // Get all remote records 
    public List<Biometrics> getAllRemoteRecords() {
        JpaRepositoryFactory factory = new JpaRepositoryFactory(remoteDatabase);
        BiometricsRepository repository = factory.getRepository(BiometricsRepository.class);
        List<Biometrics> allRemoteRecords = (List<Biometrics>) repository.findAll();
        return allRemoteRecords;
    }
    
        // Get all local records 
    public List<Biometrics> getAllLocalRecords() {
        JpaRepositoryFactory factory = new JpaRepositoryFactory(localDatabase);
        BiometricsRepository repository = factory.getRepository(BiometricsRepository.class);
        List<Biometrics> allRemoteRecords = (List<Biometrics>) repository.findAll();
        return allRemoteRecords;
    }

    // Get remote records count
    public long remoteRecordsCount() {
        JpaRepositoryFactory factory = new JpaRepositoryFactory(remoteDatabase);
        BiometricsRepository repository = factory.getRepository(BiometricsRepository.class);
        long recordsCount = repository.count();
        return recordsCount;
    }

    // Get local records count
    public long localRecordsCount() {
        JpaRepositoryFactory factory = new JpaRepositoryFactory(localDatabase);
        BiometricsRepository repository = factory.getRepository(BiometricsRepository.class);
        long recordsCount = repository.count();
        return recordsCount;
    }

}
