/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ams.springboot.db_sync.scheduler;

import com.ams.springboot.db_sync.models.HrStaffResumeFileInfo;
import com.ams.springboot.db_sync.repository.HrStaffResumeFileInfoRepository;
import com.ams.springboot.db_sync.repository.HrStaffAttendRepository;
import com.ams.springboot.db_sync.utils.EntityManagerUtils;
import java.util.List;
import javax.persistence.EntityManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.support.JpaRepositoryFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Gideon Karuga 
 */
@Controller
@Transactional
public class HrStaffResumeFileInfoScheduler {
    
    
    private static final org.apache.log4j.Logger LOGGER = org.apache.log4j.Logger.getLogger(HrStaffResumeFileInfoScheduler.class);

    @Autowired
    @Qualifier("localEntityManager")
    private EntityManager localDatabase;

    @Autowired
    @Qualifier("remoteEntityManager")
    private EntityManager remoteDatabase;

    @Autowired
    private EntityManagerUtils emUtils;

    @Autowired
    HrStaffAttendRepository hrStaffAttendRepository;

    @Scheduled(initialDelay = 1000, fixedRate = 2000)
    public void syncHrStaffAttenDbRemoteToLocal() {

        LOGGER.info("Syncing Hr Staff Resume File info [REMOTE TO LOCAL]....");
        
        
        // Get remote data count 
        // Compare with local 
        // If remote db has more records, sync to local 
        
         // Get remote data count 
        long remoteRecordsCount = remoteRecordsCount();

        // Get local data count 
        long localRecordsCount = localRecordsCount();

        LOGGER.info("remoteRecordsCount : " + remoteRecordsCount + ", localRecordsCount : " + localRecordsCount);

        // If remote db has more records, sync to local 
        if (remoteRecordsCount > localRecordsCount) {
            LOGGER.info("Syncing Hr Staff Resume File data [REMOTE TO LOCAL]...");
            
            List<HrStaffResumeFileInfo> remoteRecords = getAllRemoteRecords(); 
            
            for (HrStaffResumeFileInfo info: remoteRecords){
                 HrStaffResumeFileInfo localRecord = getLocalRecord(info.getHrmId());
                if (localRecord == null) {
                    String savedHrmId = saveRecord(info, localDatabase);
                    LOGGER.info("Synced staff info data >>> " + savedHrmId);
                }
                
            }
        }
        

    }
    
    
    @Scheduled(initialDelay = 1000, fixedRate = 2000)
    public void syncHrStaffAttenDbLocalToRemote() {

        LOGGER.info("Syncing Hr Staff Resume File info [LOCAL TO REMOTE]....");
        
        
        // Get remote data count 
        // Compare with local 
        // If remote db has more records, sync to local 
        
         // Get remote data count 
        long remoteRecordsCount = remoteRecordsCount();

        // Get local data count 
        long localRecordsCount = localRecordsCount();

        LOGGER.info("remoteRecordsCount : " + remoteRecordsCount + ", localRecordsCount : " + localRecordsCount);

        // If remote db has more records, sync to local 
        if (localRecordsCount  > remoteRecordsCount ) {
            LOGGER.info("Syncing Hr Staff Resume File data [LOCAL TO REMOTE]...");
            
            List<HrStaffResumeFileInfo> localRecords = getAllLocalRecords(); 
            
            for (HrStaffResumeFileInfo info: localRecords){
                 HrStaffResumeFileInfo remoteRecord = getRemoteRecord(info.getHrmId());
                if (remoteRecord == null) {
                    String savedHrmId = saveRecord(info, remoteDatabase);
                    LOGGER.info("Synced staff info data >>> " + savedHrmId);
                }
                
            }
        }
        

    }
    
     // Save remote record to local server 
    public String saveRecord(HrStaffResumeFileInfo info, EntityManager databaseLocation) {
        
        
        HrStaffResumeFileInfo newInfo = new HrStaffResumeFileInfo();
        
        newInfo.setHrmId(info.getHrmId());
        newInfo.setInterviewDate(info.getInterviewDate());
        newInfo.setFirstName(info.getFirstName());
        newInfo.setSecondName(info.getSecondName());
        newInfo.setOthers(info.getOthers());
        newInfo.setStaffId(info.getStaffId());
        newInfo.setEmpmtdate(info.getEmpmtdate());
        newInfo.setFieldOfSpec(info.getFieldOfSpec());
        newInfo.setPositionApp(info.getPositionApp());
        newInfo.setDept(info.getDept());
        newInfo.setBirthDate(info.getBirthDate());
        newInfo.setCurrentResidence(info.getCurrentResidence());
        newInfo.setNationalIdNo(info.getNationalIdNo());
        newInfo.setPassportNo(info.getPassportNo());
        newInfo.setMaritalStatus(info.getMaritalStatus());
        newInfo.setReligion(info.getReligion());
        newInfo.setNoticePeriod(info.getNoticePeriod());
        newInfo.setEthnicity(info.getEthnicity());
        newInfo.setGender(info.getGender());
        newInfo.setHelb(info.getHelb());
        newInfo.setNssfNo(info.getNssfNo());
        newInfo.setNhifNo(info.getNhifNo());
        newInfo.setPostalAdd(info.getPostalAdd());
        newInfo.setPostalCode(info.getPostalCode());
        newInfo.setEmail(info.getEmail());
        newInfo.setDesignation(info.getDesignation());
        newInfo.setSection(info.getSection());
        newInfo.setJobGroup(info.getJobGroup());
        newInfo.setJgrade(info.getJgrade());
        newInfo.setNationality(info.getNationality());
        newInfo.setBirthPlace(info.getBirthPlace());
        newInfo.setBirthDistrict(info.getBirthDistrict());
        newInfo.setBirthTown(info.getBirthTown());
        newInfo.setPinNo(info.getPinNo());
        newInfo.setMobile(info.getMobile());
        newInfo.setMobile2(info.getMobile2());
        newInfo.setEmerContnames(info.getEmerContnames());
        newInfo.setEmerRelationship(info.getEmerRelationship());
        newInfo.setEmerRelationship(info.getEmerRelationship());
        newInfo.setStaffCategory(info.getStaffCategory());
        newInfo.setLeaveCategory(info.getLeaveCategory());
        newInfo.setEmerMobile(info.getEmerMobile());
        newInfo.setEmerEmail(info.getEmerEmail());
        newInfo.setEmerPostaladd(info.getEmerPostaladd());
        newInfo.setEmerPostalcode(info.getEmerPostalcode());
        newInfo.setEmerResidence(info.getEmerResidence());
        newInfo.setConsultant(info.isConsultant());
        newInfo.setSystemRights(info.isSystemRights());
        newInfo.setContractRenew(info.isContractRenew());
        newInfo.setContractRermination(info.isContractRermination());
        newInfo.setContractCreate(info.isContractCreate());
        newInfo.setReportDate(info.getReportDate());
        newInfo.setPayrollStatus(info.isPayrollStatus());
        newInfo.setStaffStatus(info.getStaffStatus());
        newInfo.setHrmId(info.getHrmId());
        newInfo.setConfirmed(info.isConfirmed());
        newInfo.setSuspend(info.isSuspend());
        newInfo.setOnleave(info.isOnleave());
        newInfo.setStandardShift(info.isStandardShift());
        newInfo.setInputDate(info.getInputDate());
        newInfo.setUserName(info.getUserName());
        newInfo.setApproved(info.isApproved());
        newInfo.setApprovalStatus(info.getApprovalStatus());
        newInfo.setWorkEmail(info.getWorkEmail());
        newInfo.setChecked(info.isChecked());
        newInfo.setEditedBy(info.getEditedBy());
        newInfo.setEditedDate(info.getEditedDate());
        newInfo.setHod(info.isHod());
        newInfo.setSupervisor(info.getSupervisor());
        newInfo.setSpecial(info.isSpecial());
        newInfo.setCompanyId(info.getCompanyId());
        newInfo.setTitle(info.getTitle());
        newInfo.setJob(info.getJob());
        newInfo.setEmpCategory(info.getEmpCategory());
        newInfo.setBunit(info.getBunit());
        newInfo.setPayroll(info.getPayroll());
        newInfo.setEmpCode(info.getEmpCode());
        newInfo.setBranchEnabled(info.isBranchEnabled());
        
        
        JpaRepositoryFactory factory = new JpaRepositoryFactory(databaseLocation);
        HrStaffResumeFileInfoRepository repository = factory.getRepository(HrStaffResumeFileInfoRepository.class);
        repository.save(newInfo);
        
        return newInfo.getHrmId();
        
    }
    
   
    public HrStaffResumeFileInfo getLocalRecord(String hrmid) {
        JpaRepositoryFactory factory = new JpaRepositoryFactory(localDatabase);
        HrStaffResumeFileInfoRepository repository = factory.getRepository(HrStaffResumeFileInfoRepository.class);
        HrStaffResumeFileInfo info = repository.findByHrmId(hrmid);
        return info;
    }
    
    public HrStaffResumeFileInfo getRemoteRecord(String hrmid) {
        JpaRepositoryFactory factory = new JpaRepositoryFactory(remoteDatabase);
        HrStaffResumeFileInfoRepository repository = factory.getRepository(HrStaffResumeFileInfoRepository.class);
        HrStaffResumeFileInfo info = repository.findByHrmId(hrmid);
        return info;
    }

    public List<HrStaffResumeFileInfo> getAllRemoteRecords() {
        JpaRepositoryFactory factory = new JpaRepositoryFactory(remoteDatabase);
        HrStaffResumeFileInfoRepository repository = factory.getRepository(HrStaffResumeFileInfoRepository.class);
        List<HrStaffResumeFileInfo> allRemoteRecords = (List<HrStaffResumeFileInfo>) repository.findAll();
        return allRemoteRecords;
    }
    
     public List<HrStaffResumeFileInfo> getAllLocalRecords() {
        JpaRepositoryFactory factory = new JpaRepositoryFactory(localDatabase);
        HrStaffResumeFileInfoRepository repository = factory.getRepository(HrStaffResumeFileInfoRepository.class);
        List<HrStaffResumeFileInfo> allRemoteRecords = (List<HrStaffResumeFileInfo>) repository.findAll();
        return allRemoteRecords;
    }
    
      // Get remote records count
    public long remoteRecordsCount() {
        JpaRepositoryFactory factory = new JpaRepositoryFactory(remoteDatabase);
        HrStaffResumeFileInfoRepository repository = factory.getRepository(HrStaffResumeFileInfoRepository.class);
        long recordsCount = repository.count();
        return recordsCount;
    }

    // Get local records count
    public long localRecordsCount() {
        JpaRepositoryFactory factory = new JpaRepositoryFactory(localDatabase);
        HrStaffResumeFileInfoRepository repository = factory.getRepository(HrStaffResumeFileInfoRepository.class);
        long recordsCount = repository.count();
        return recordsCount;
    }


    
}
