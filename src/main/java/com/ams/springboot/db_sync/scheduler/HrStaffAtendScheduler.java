/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ams.springboot.db_sync.scheduler;

import java.util.List;
import com.ams.springboot.db_sync.models.HrStaffAttend;
import com.ams.springboot.db_sync.repository.BiometricsRepository;
import com.ams.springboot.db_sync.repository.HrStaffAttendRepository;
import com.ams.springboot.db_sync.utils.EntityManagerUtils;
import javax.persistence.EntityManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.support.JpaRepositoryFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Gideon Karuga.
 */
@Controller
@Transactional
public class HrStaffAtendScheduler {

    private static final org.apache.log4j.Logger LOGGER = org.apache.log4j.Logger.getLogger(HrStaffAtendScheduler.class);

    @Autowired
    @Qualifier("localEntityManager")
    private EntityManager localDatabase;

    @Autowired
    @Qualifier("remoteEntityManager")
    private EntityManager remoteDatabase;

    @Autowired
    private EntityManagerUtils emUtils;

    @Autowired
    HrStaffAttendRepository hrStaffAttendRepository;

    @Scheduled(initialDelay = 1000, fixedRate = 2000)
    public void syncHrStaffAttenDbLocalToRemote() {

        long localRecordsCount = getHrStaffAttendRecordsCount(localDatabase);
        long remoteRecordsCount = getHrStaffAttendRecordsCount(remoteDatabase);        
        
        if (localRecordsCount > remoteRecordsCount){
            
            List<HrStaffAttend> localHrStaffAttendance = getAllStaffAttendRecords(localDatabase);            
            localHrStaffAttendance.stream().forEach((staffAttend) -> {

            // Local record 
            LOGGER.info("StaffAttend log ID >>> " + staffAttend.getLogId());
            // Check if value exists in remote server        
            // If it does not exist, save to remote server
            HrStaffAttend remoteHrStaffAttend = getRecord(staffAttend.getLogId(), remoteDatabase);

            if (remoteHrStaffAttend == null) {
                LOGGER.info("Saving local [HrStaffAttend] record to remote server....");
                String saveResult = saveRecord(staffAttend, remoteDatabase);
                LOGGER.info("saveResult >>> " + saveResult);

                if (saveResult.trim().length() > 0) {
                    markRecordAsSynced(staffAttend, remoteDatabase);
                }
            } else {
                LOGGER.info("Record already exists..");
            }
        });
        }
    }

    @Scheduled(initialDelay = 1000, fixedRate = 2000)
    public void syncHrStaffAttenDbRemoteToLocal() {
        
        long remoteRecordsCount = getHrStaffAttendRecordsCount(remoteDatabase);        
        long localRecordsCount = getHrStaffAttendRecordsCount(localDatabase);
        
        if (remoteRecordsCount > localRecordsCount){
            
            List<HrStaffAttend> remoteHrStaffAttendance = getAllStaffAttendRecords(remoteDatabase);            
            remoteHrStaffAttendance.stream().forEach((staffAttend) -> {

            LOGGER.info("StaffAttend log ID >>> " + staffAttend.getLogId());            
            HrStaffAttend localHrStaffAttend = getRecord(staffAttend.getLogId(), localDatabase);

            if (localHrStaffAttend == null) {
                LOGGER.info("Saving remote [HrStaffAttend] record to local server....");
                String saveResult = saveRecord(staffAttend, localDatabase);
                LOGGER.info("saveResult >>> " + saveResult);

                if (saveResult.trim().length() > 0) {
                    markRecordAsSynced(staffAttend, localDatabase);
                }
            } else {
                LOGGER.info("Record already exists..");
            }
        });
        }
    }

    
    public long getHrStaffAttendRecordsCount(EntityManager databaseLocation) {
        JpaRepositoryFactory factory = new JpaRepositoryFactory(databaseLocation);
        HrStaffAttendRepository repository = factory.getRepository(HrStaffAttendRepository.class);
        long recordsCount = repository.count();
        return recordsCount;
    }

    public List<HrStaffAttend> getAllStaffAttendRecords(EntityManager databaseLocation) {
        JpaRepositoryFactory factory = new JpaRepositoryFactory(databaseLocation);
        HrStaffAttendRepository repository = factory.getRepository(HrStaffAttendRepository.class);
        List<HrStaffAttend> hrStaffAttendance = (List<HrStaffAttend>) repository.findAll();
        return hrStaffAttendance;
    }
    
    // Get all locally saved records that have not been synced yet 
    public List<HrStaffAttend> getAllLocalNonSyncedData() {
        JpaRepositoryFactory factory = new JpaRepositoryFactory(localDatabase);
        HrStaffAttendRepository repository = factory.getRepository(HrStaffAttendRepository.class);
        List<HrStaffAttend> hrStaffAttendance = (List<HrStaffAttend>) repository.findByIsSynced(false);
        return hrStaffAttendance;
    }

    // markLocalRecordAsSynced
    public void markRecordAsSynced(HrStaffAttend hrStaffAttendance, EntityManager databaseLocation) {
        // HrStaffAttendRepository repository = emUtils.getJpaFactory("local").getRepository(HrStaffAttendRepository.class);
        JpaRepositoryFactory factory = new JpaRepositoryFactory(databaseLocation);
        HrStaffAttendRepository repository = factory.getRepository(HrStaffAttendRepository.class);
        hrStaffAttendance.setIsSynced(true);
        repository.save(hrStaffAttendance);
    }

    public HrStaffAttend getRecord(String logId, EntityManager databaseLocation) {
        JpaRepositoryFactory factory = new JpaRepositoryFactory(databaseLocation);
        HrStaffAttendRepository repository = factory.getRepository(HrStaffAttendRepository.class);
        HrStaffAttend hrStaffAttendance = repository.findByLogId(logId);
        return hrStaffAttendance;
    }

    // Save local record to remote server       
    public String saveRecord(HrStaffAttend hrStaffAttendance, EntityManager databaseLocation) {

        HrStaffAttend newRecord = new HrStaffAttend();

        newRecord.setStaffNo(hrStaffAttendance.getStaffNo());
        newRecord.setStaffName(hrStaffAttendance.getStaffName());
        newRecord.setAttendTime(hrStaffAttendance.getAttendTime());
        newRecord.setLogId(hrStaffAttendance.getLogId());
        newRecord.setDept(hrStaffAttendance.getDept());
        newRecord.setLogName(hrStaffAttendance.getLogName());
        newRecord.setCheckedOut(hrStaffAttendance.getCheckedOut());
        newRecord.setCompanyId(hrStaffAttendance.getCompanyId());
        newRecord.setClockOut(hrStaffAttendance.getClockOut());
        newRecord.setEndTime(hrStaffAttendance.getEndTime());
        newRecord.setCompName(hrStaffAttendance.getCompName());
        newRecord.setLogOut(hrStaffAttendance.getLogOut());
        newRecord.setSmsLateness(hrStaffAttendance.isSmsLateness());
        newRecord.setSmsEarlybird(hrStaffAttendance.isSmsEarlybird());
        newRecord.setAutomatedClockin(hrStaffAttendance.isAutomatedClockin());
        newRecord.setPayExempt(hrStaffAttendance.isPayExempt());
        newRecord.setEditReason(hrStaffAttendance.getEditReason());
        newRecord.setMacAddress(hrStaffAttendance.getMacAddress());
        newRecord.setInputDate(hrStaffAttendance.getInputDate());
        newRecord.setCompanyId(hrStaffAttendance.getCompanyId());
        newRecord.setIsSynced(true);

        // User remote repository for saving to remote server 
        // JpaRepositoryFactory factory = new JpaRepositoryFactory(remoteDatabase);
        // HrStaffAttendRepository repository = factory.getRepository(HrStaffAttendRepository.class);
        // HrStaffAttend response = repository.save(newRecord);
        // remoteDatabase.persist(newRecord);

        hrStaffAttendRepository.save(newRecord);

        return newRecord.getLogId();
    }

}
