/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ams.springboot.db_sync.rest;

import com.ams.springboot.db_sync.models.ApiReponse;
import com.ams.springboot.db_sync.models.Biometrics;
import com.ams.springboot.db_sync.models.HrStaffAttend;
import com.ams.springboot.db_sync.repository.BiometricsRepository;
import com.ams.springboot.db_sync.repository.HrStaffAttendRepository;
import com.ams.springboot.db_sync.scheduler.HrStaffAtendScheduler;
import com.ams.springboot.db_sync.utils.EntityManagerUtils;
import java.util.Date;
import javax.persistence.EntityManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.support.JpaRepositoryFactory;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Transactional
@RequestMapping(value = "/hrstaffattend")
public class HrStaffAttendRest {

    private static final org.apache.log4j.Logger LOGGER = org.apache.log4j.Logger.getLogger(HrStaffAttendRest.class);

    @Autowired
    private HrStaffAttendRepository repository;

    @Autowired
    @Qualifier("localEntityManager")
    private EntityManager localDatabase;

    @Autowired
    @Qualifier("remoteEntityManager")
    private EntityManager remoteDatabase;

    @Autowired
    private EntityManagerUtils emUtils;

    @Autowired
    private HrStaffAtendScheduler hrStaffAtendScheduler;

    @RequestMapping(value = {"/health_check"}, method = RequestMethod.GET)
    public ApiReponse healthCheck() {
        ApiReponse apiResponse = new ApiReponse();
        apiResponse.setStatus("00");
        apiResponse.setDescription("Ok. Sevices Up.");
        return apiResponse;
    }

    @RequestMapping(value = "/all")
    public Iterable<HrStaffAttend> getAll() {
        return repository.findAll();
    }

    @RequestMapping(value = {"/clock_out/{user_id}/{log_id}"}, method = RequestMethod.GET)
    public ApiReponse staffClockOut(@PathVariable String user_id, @PathVariable String log_id) {

        ApiReponse apiResponse = new ApiReponse();
        LOGGER.info("Clocking out user >>> " + user_id + ", Log ID : " + log_id);

        HrStaffAttend localRecord = hrStaffAtendScheduler.getRecord(log_id, localDatabase);
               
        if (localRecord == null ) {
            apiResponse.setStatus("01");
            apiResponse.setDescription("Local record not found!");
            return apiResponse;
        } else {
            // Sync data - retrieve remote record 
            HrStaffAttend remoteHrStaffAttend = hrStaffAtendScheduler.getRecord(log_id, remoteDatabase);
            
            if (remoteHrStaffAttend != null){
                
                String saveResult = hrStaffAtendScheduler.saveRecord(remoteHrStaffAttend, localDatabase);
                LOGGER.info("saveResult >>> " + saveResult);

                if (saveResult.trim().length() > 0) {
                    hrStaffAtendScheduler.markRecordAsSynced(remoteHrStaffAttend, remoteDatabase);
                }
            }
        }
        
        // Update hr staff attend table - local and remote 
        // UPDATE hr_staff_attend set checked_out=true,clock_out=current_timestamp,end_time=current_timestamp,log_out=current_timestamp WHERE staff_no='" + this.userID + "' and log_id='" + this.logId + "'
        updateLocalHrStaffAttendRecord(log_id);

        localRecord = hrStaffAtendScheduler.getRecord(log_id, localDatabase);

        if (localRecord.getCheckedOut() == false) {
            apiResponse.setStatus("01");
            apiResponse.setDescription("Error clocking out user! Local Record Save Error. ");
            return apiResponse;
        }
        
        updateRemoteHrStaffAttendRecord(log_id);
        
        // Update biometrics table - local and remote 
        // UPDATE biometrics SET is_user_clocked=false WHERE username_id='" + this.userID + "' AND log_id='" + this.logId + "                
        updateLocalBiometricRecord(user_id, log_id);
        updateRemoteBiometricRecord(user_id, log_id);
        apiResponse.setStatus("00");
        apiResponse.setDescription("User Clocked Out Successfully.");

        return apiResponse;
    }

    // UPDATE hr_staff_attend set checked_out=true,clock_out=current_timestamp,end_time=current_timestamp,log_out=current_timestamp WHERE staff_no='" + this.userID + "' and log_id='" + this.logId + "'
    // updateLoccalHrStaffAttendRecord
    public HrStaffAttend updateLocalHrStaffAttendRecord(String logId) {
        JpaRepositoryFactory factory = new JpaRepositoryFactory(localDatabase);
        HrStaffAttendRepository repository = emUtils.getJpaFactory("local").getRepository(HrStaffAttendRepository.class);
        // HrStaffAttendRepository repository = factory.getRepository(HrStaffAttendRepository.class);
        HrStaffAttend hrStaffAttendance = repository.findByLogId(logId);

        if (hrStaffAttendance != null) {
            LOGGER.info("HrStaffAttendance Attend Time [LOCAL] >>> " + hrStaffAttendance.getAttendTime());
            hrStaffAttendance.setCheckedOut(true);
            hrStaffAttendance.setClockOut(new Date());
            hrStaffAttendance.setEndTime(new Date());
            hrStaffAttendance.setLogOut(new Date());
            hrStaffAttendance.setIsSynced(false);
            repository.save(hrStaffAttendance);
        }
        return hrStaffAttendance;
    }

    // updateRemoteHrStaffAttendRecord
    public HrStaffAttend updateRemoteHrStaffAttendRecord(String logId) {
        JpaRepositoryFactory factory = new JpaRepositoryFactory(remoteDatabase);
        HrStaffAttendRepository repository = factory.getRepository(HrStaffAttendRepository.class);
        HrStaffAttend hrStaffAttendance = repository.findByLogId(logId);
        if (hrStaffAttendance != null) {
            LOGGER.info("HrStaffAttendance Attend Time [REMOTE] >>> " + hrStaffAttendance.getAttendTime());
            hrStaffAttendance.setCheckedOut(true);
            hrStaffAttendance.setClockOut(new Date());
            hrStaffAttendance.setEndTime(new Date());
            hrStaffAttendance.setLogOut(new Date());
            repository.save(hrStaffAttendance);
        }
        return hrStaffAttendance;
    }

    // UPDATE biometrics SET is_user_clocked=false WHERE username_id='" + this.userID + "' AND log_id='" + this.logId + "                
    public Biometrics updateLocalBiometricRecord(String userId, String logId) {
        JpaRepositoryFactory factory = new JpaRepositoryFactory(localDatabase);
        BiometricsRepository repository = factory.getRepository(BiometricsRepository.class);
        // WHERE username_id='" + this.userID + "' AND log_id='" + this.logId + "         
        Biometrics biometric = repository.findByUsernameIdAndLogId(userId, logId);
        if (biometric != null) {
            LOGGER.info("Biometric ID [LOCAL] >>> " + biometric.getId());
            biometric.setIsUserClocked(false);
            repository.save(biometric);
        }
        return biometric;
    }

    public Biometrics updateRemoteBiometricRecord(String userId, String logId) {
        JpaRepositoryFactory factory = new JpaRepositoryFactory(remoteDatabase);
        BiometricsRepository repository = factory.getRepository(BiometricsRepository.class);
        // WHERE username_id='" + this.userID + "' AND log_id='" + this.logId + "         
        Biometrics biometric = repository.findByUsernameIdAndLogId(userId, logId);
        if (biometric != null) {
            LOGGER.info("Biometric ID [REMOTE] >>> " + biometric.getId());
            biometric.setIsUserClocked(false);
            repository.save(biometric);
        }
        return biometric;
    }
}
