/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ams.springboot.db_sync.repository;

import com.ams.springboot.db_sync.models.HrStaffResumeFileInfo;

/**
 *
 * @author Gideon Karuga. 
 */

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

@Service
public interface HrStaffResumeFileInfoRepository extends CrudRepository<HrStaffResumeFileInfo, Long> {

    HrStaffResumeFileInfo findByHrmId(String hrmid);

}
