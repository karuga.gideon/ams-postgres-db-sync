/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ams.springboot.db_sync.repository;

/**
 *
 * @author Gideon Karuga.
 */
import com.ams.springboot.db_sync.models.Biometrics;
import java.math.BigInteger;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

@Service
public interface BiometricsRepository extends CrudRepository<Biometrics, Long> {

    Biometrics findByLogId(String logId);

    Biometrics findById(BigInteger id);

    Biometrics findByUsernameIdAndLogId(String usernameId, String logId);

}
