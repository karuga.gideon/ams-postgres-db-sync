/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ams.springboot.db_sync.repository;

import com.ams.springboot.db_sync.models.HrStaffAttend;
import java.util.List;
// import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

@Service
public interface HrStaffAttendRepository extends CrudRepository<HrStaffAttend, Long> {

    HrStaffAttend findByLogId(String logId);

    List<HrStaffAttend> findByIsSynced(boolean isSynced);
}
